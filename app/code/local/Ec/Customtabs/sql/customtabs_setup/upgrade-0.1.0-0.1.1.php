<?php

$installer = $this;
$installer->startSetup();

$installer->run("
		ALTER TABLE `{$this->getTable('catalog_eav_attribute')}` 
		add column `default_tab` smallint(5) DEFAULT '0' NOT NULL,
		add column `customtabs` varchar(255) NULL;
    ");

$installer->endSetup();