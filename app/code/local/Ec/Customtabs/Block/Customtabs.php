<?php
class Ec_Customtabs_Block_Customtabs extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCustomtabs()     
     { 
        if (!$this->hasData('customtabs')) {
            $this->setData('customtabs', Mage::registry('customtabs'));
        }
        return $this->getData('customtabs');
        
    }
}