<?php
class Ec_Customtabs_Block_Product_Attributes extends CLS_Custom_Block_Catalog_Product_View_Attributes
{
	
    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     */
    public function getAdditionalData(array $excludeAttr = array())
    {
        if (!Mage::getStoreConfig('catalog/hideempty/poweroptions')) {
            return parent::getAdditionalData($excludeAttr);
        }
        
        $data = array();
        //$customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
		$customerGroupId = Mage::getSingleton('customer/session')->getUserCustomer()->getGroupId();
		
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
//            if ($attribute->getIsVisibleOnFront() && $attribute->getIsUserDefined() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
				if(!$attribute->getDefaultTab()) {
					$value = $attribute->getFrontend()->getValue($product);

					if (Mage::getStoreConfig('catalog/hideemptyattributes/booleanattr')
						&& $attribute->getFrontendInput() == 'boolean'
						&& !$product->hasData($attribute->getAttributeCode())) {
						$value = "";
					} elseif ($attribute->getFrontendInput() == 'select'
						&& !$product->getAttributeText($attribute->getAttributeCode())
					) {
						$value = "";
					} elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
						$value = Mage::app()->getStore()->convertPrice($value, true);
					}

					if (is_string($value) && strlen(trim($value))) {
						$data[$attribute->getAttributeCode()] = array(
							'label' => $attribute->getStoreLabel(),
							'value' => $value,
							'code'  => $attribute->getAttributeCode()
						);
					}

					//check for if attribute is show to customer group all
					if (!$attribute->getShowToAllCustomerGroups()) {
						$groups = explode(',',$attribute->getCustomerGroupsToShow());

						//if do not show to this customer group remove from data
						if (!in_array($customerGroupId, $groups)) {
							unset($data[$attribute->getAttributeCode()]);
						}
					}					
				}	
            }
        }
        return $data;
    }	
	
    public function getAdditionalCustomData(array $excludeAttr = array(), $tabId)
    {
        $data = array();
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
			
			if($attribute->getDefaultTab() && $attribute->getCustomtabs()) {
				$customTabs = explode(',', $attribute->getCustomtabs());
				if(in_array($tabId, $customTabs)) {
					
					if($attribute->getShowToAllCustomerGroups() == 0) {					
						$allowedGroups = explode(',', $attribute->getCustomerGroupsToShow());
						
						$customerId = 0;
						if(Mage::getSingleton('customer/session')->isLoggedIn()) {
							$customerId = Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
						}						
						
						if(!in_array($customerId, $allowedGroups)) {
							continue;
						}	
					}	
					
					if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
						$value = $attribute->getFrontend()->getValue($product);

						if (!$product->hasData($attribute->getAttributeCode())) {
							$value = Mage::helper('catalog')->__('N/A');
						} elseif ((string)$value == '') {
							$value = Mage::helper('catalog')->__('No');
						} elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
							$value = Mage::app()->getStore()->convertPrice($value, true);
						}

						if (is_string($value) && strlen($value)) {
							$data[$attribute->getAttributeCode()] = array(
								'label' => $attribute->getStoreLabel(),
								'value' => $value,
								'code'  => $attribute->getAttributeCode()
							);
						}
					}					
				}
			}			
			else {
				continue;
			}	
        }
		
        return $data;
    }	

}