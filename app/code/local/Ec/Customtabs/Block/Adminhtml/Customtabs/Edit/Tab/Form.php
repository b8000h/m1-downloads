<?php

class Ec_Customtabs_Block_Adminhtml_Customtabs_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('customtabs_form', array('legend'=>Mage::helper('customtabs')->__('Tab information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('customtabs')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('customtabs')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('customtabs')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('customtabs')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getCustomtabsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCustomtabsData());
          Mage::getSingleton('adminhtml/session')->setCustomtabsData(null);
      } elseif ( Mage::registry('customtabs_data') ) {
          $form->setValues(Mage::registry('customtabs_data')->getData());
      }
      return parent::_prepareForm();
  }
}