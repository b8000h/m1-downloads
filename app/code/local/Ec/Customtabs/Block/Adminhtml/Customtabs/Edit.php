<?php

class Ec_Customtabs_Block_Adminhtml_Customtabs_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'customtabs';
        $this->_controller = 'adminhtml_customtabs';
        
        $this->_updateButton('save', 'label', Mage::helper('customtabs')->__('Save Tab'));
        $this->_updateButton('delete', 'label', Mage::helper('customtabs')->__('Delete Tab'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('customtabs_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'customtabs_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'customtabs_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('customtabs_data') && Mage::registry('customtabs_data')->getId() ) {
            return Mage::helper('customtabs')->__("Edit Tab '%s'", $this->htmlEscape(Mage::registry('customtabs_data')->getTitle()));
        } else {
            return Mage::helper('customtabs')->__('Add Tab');
        }
    }
}