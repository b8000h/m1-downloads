<?php

class Ec_Customtabs_Block_Adminhtml_Customtabs_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('customtabs_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('customtabs')->__('Tab Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('customtabs')->__('Tab Information'),
          'title'     => Mage::helper('customtabs')->__('Tab Information'),
          'content'   => $this->getLayout()->createBlock('customtabs/adminhtml_customtabs_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}