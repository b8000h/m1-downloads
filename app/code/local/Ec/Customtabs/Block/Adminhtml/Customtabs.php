<?php
class Ec_Customtabs_Block_Adminhtml_Customtabs extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_customtabs';
    $this->_blockGroup = 'customtabs';
    $this->_headerText = Mage::helper('customtabs')->__('Tabs Manager');
    $this->_addButtonLabel = Mage::helper('customtabs')->__('Add Tab');
    parent::__construct();
  }
}