<?php

class Ec_Customtabs_Model_Mysql4_Customtabs extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the customtabs_id refers to the key field in your database table.
        $this->_init('customtabs/customtabs', 'customtabs_id');
    }
}