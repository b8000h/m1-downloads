<?php

class Ec_Customtabs_Model_Mysql4_Customtabs_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('customtabs/customtabs');
    }
}