<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_CustomerSegment
 * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * CustomerSegment observer
 *
 */
class Ec_Customtabs_Model_Observer
{

    public function setTabsData(Varien_Event_Observer $observer)
    {
		$object = $observer->getEvent()->getAttribute();
		
		if($object->getDefaultTab() == 1) {
			if($object->getCustomtabs()) {
				$impCustomtabs = implode(',',$object->getCustomtabs());
				$object->setCustomtabs($impCustomtabs);			
			}				
		}
		else {
			$object->setCustomtabs(NULL);			
		}		
			
	}	
	
    public function addTabsFields(Varien_Event_Observer $observer)
    {
		$fieldset = $observer->getForm()->getElement('front_fieldset');
		
		$allowedTabs = array();
		$tabsCollection = Mage::getModel('customtabs/customtabs')->getCollection();
		if(count($tabsCollection) > 0) {			
			foreach($tabsCollection	as $eachTab) {
				$allowedTabs[] = array(
									'label' => $eachTab->getTitle(),
									'value' => $eachTab->getCustomtabsId(),
									);
			}
		}	

		$yesnoSource = array();
		$yesnoSource[] = array(
							'value' => 0,
							'label' => 'Yes',
							);
		$yesnoSource[] = array(
							'value' => 1,
							'label' => 'No',
							);							
		
        $fieldset->addField('default_tab', 'select', array(
            'name'	=>	'default_tab',
            'label'	=>	Mage::helper('catalog')->__('Use Default Tab?'),
            'title'	=>	Mage::helper('catalog')->__('Use Default Tab?'),
            'values'=>	$yesnoSource,
			'note'	=>	'If set to No, Show under Tabs will need to be configured',	
        ));				
		
        $fieldset->addField('customtabs', 'multiselect', array(
            'name'	=>	'customtabs[]',
            'label'	=>	Mage::helper('catalog')->__('Show under Tabs'),
			'title'	=>	Mage::helper('catalog')->__('Show under Tabs'),
            'values'	=>	$allowedTabs,
            'required'	=>	false,
			'note'	=>	'This is only needed if Use Default Tab? is set to No',	
        ));		
		
		$formAfter = $observer->getForm()->getParent()->getChild('form_after');
        $formAfter->addFieldMap('default_tab','default_tab');
		$formAfter->addFieldMap('customtabs','customtabs');
        $formAfter->addFieldDependence('customtabs', 'default_tab', '1');
    }

}
